﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

    public List<Evento> CurrentEventsInTheScene { private set; get; }
    public Dictionary<string, Evento> CurrentEvents { private set; get; }
    public bool bloccoTaglio { private set; get; } // true: BLOCCO 		false: TAGLIO
    public LineRenderer player { set; get; }
    [SerializeField] private Material material, material2;
    [SerializeField] private RectTransform buttonsTaglioBlocco;
    [SerializeField] private AnimationManager AnimationManager;
    public int id_event { get; set; }
    // Use this for initialization
    void Start () {
        bloccoTaglio = false;
        CurrentEventsInTheScene = new List<Evento> ();
        CurrentEvents = new Dictionary<string, Evento> ();
        id_event = 0;
    }

    // Update is called once per frame
    void Update () {

    }
    public void AddToCurrentEvents (Evento e) { // uso questo per il passaggio

        // nel caso in cui ho già spostato la palla una volta, aggiorno il ricevitore all'evento del passaggio
        // e SE non esiste quel passaggio, inserisco l'evento.

        if (CurrentEvents.ContainsKey (e.idPlayer1)) {
            Debug.Log ("vecchio: " + CurrentEvents[e.idPlayer1].idPlayer2 + " nuovo: " + e.idPlayer2);
            CurrentEvents[e.idPlayer1].idPlayer2 = e.idPlayer2;

        } else {
            CurrentEvents.Add (e.idPlayer1, e);
        }

        // CurrentEventsInTheScene.Add (e); ****** VECCHIO SISTEMA ******
        id_event++;

        // Debug.Log("EVENT" + " ---> " + 
        //           e.id_type.ToString() + "," + 
        //           e.idPlayer1.ToString() + "," +
        //           e.idPlayer2.ToString() + "," +
        //           Mathf.Abs(e.duration).ToString());

    }

    public void AddToCurrentEvents (string player, Evento e) { // uso questo per il blocco
        CurrentEvents.Add (player, e);
        id_event++;
    }

    public void ResetList () {
        CurrentEventsInTheScene.Clear ();
        CurrentEvents.Clear ();
    }

    public void OnBloccoClick () {
        // PARTE di RenderLine, freccie, grafica
        bloccoTaglio = true;
        player.sharedMaterial = material;
        player.GetComponent<OnTransform> ().SetBlock (true);
        player.GetComponent<OnTransform> ().SetArrow (false);
        buttonsTaglioBlocco.gameObject.SetActive (false);

        // PARTE aggiunta EVENTO alla lista

        int frame_duration = AnimationManager.sliderSeconds - AnimationManager.lastKeyFrameSeconds;
        int keyframe = AnimationManager.GetTimeline ().frames.Count;

        // se non c'è un evento per quel giocatore, lo inserisco, dopo averlo creato

        if (!CurrentEvents.ContainsKey (player.name)) {
            Evento blocco = new Evento ("blocco", id_event, false, player.name, player.name, frame_duration, keyframe);
            // addEvento (blocco);
            AddToCurrentEvents (player.name, blocco);
        }
    }

    public void OnTaglioClick () {
        bloccoTaglio = false;
        player.sharedMaterial = material2;
        player.GetComponent<OnTransform> ().SetArrow (true);
        player.GetComponent<OnTransform> ().SetBlock (false);
        buttonsTaglioBlocco.gameObject.SetActive (false);

        // rimuovo l'evento dalla mappa degli eventi nel caso in cui si abbia cambiato idea riguardo taglio / blocco

        if (CurrentEvents.ContainsKey (player.name)) {
            CurrentEvents.Remove (player.name);
        }
    }
}