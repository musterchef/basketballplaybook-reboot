﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using SWS;
using UnityEngine;
using UnityEngine.UI;

public class TimelineManager : MonoBehaviour {

    public GameObject keyframe_marker;
    public Dropdown Dropdown;
    public InputField InputField;
    public bool play { get; set; }
    private AnimationManager AnimationManager;
    private PathCreator PathCreator;
    private PlayersManager PlayersManager;
    private Dictionary<string, Timeline> timelines_saved;
    private Slider timeline_slider; //timeline_slider_paused;
    private Transform handle;
    [SerializeField] private Transform keyframes;
    private GameObject Ball;
    public float differenza_slide { get; set; }
    private Vector3 marker0_position;
    private Slider speed_factor_slider;

    public bool sino = false;

    // Awake is called when the script instance is being loaded.
    void Awake () {
        this.timeline_slider = GameObject.Find ("Timeline_Slider").GetComponent<Slider> ();
        this.Ball = GameObject.Find ("Ball");
        this.PathCreator = GameObject.Find ("WaypointManager").GetComponent<PathCreator> ();
        this.AnimationManager = GameObject.Find ("AnimationManager").GetComponent<AnimationManager> ();
        this.PlayersManager = GameObject.Find ("PlayersManager").GetComponent<PlayersManager> ();
        this.handle = timeline_slider.transform.GetChild (2).transform.GetChild (0);

        // carico lo slider del velocità factor
        // this.speed_factor_slider = GameObject.Find ("Speed_Slider").GetComponent<Slider> ();
    }

    // Use this for initialization
    void Start () {
        play = false;
        differenza_slide = 0;

        // salvo x y del timeline per poi aggiungere i marker nel load

        marker0_position = handle.transform.position;
    }

    // Update is called once per frame
    void Update () {

        // CHECK IF THE TIMELINE IS PLAYING

        // if (play == true) {
        //     OnPlay ();
        // }
    }

    // use this method to create a keyframe marker when a keyframe is created, using the position on the slider handle
    // is created a new one but yellow and positioned there
    public void AddMarker () {
        GameObject s = Instantiate (keyframe_marker) as GameObject;
        s.transform.position = handle.transform.position;
        s.transform.parent = keyframes.transform;
    }

    public void OnPlay () {

        // vedo quanto tempo è passato da quando ho premuto play per far muovere lo slide da solo

        // splineMove sm = PlayersManager.GetHomePlayerInTheSceneList ()[0].GetComponent<splineMove> ();
        // if (sm.tween.IsPlaying () == true) {
        //     float time = Time.time - PathCreator.time_started_play + PathCreator.pause_time;
        //     //PathCreator.time_elapsed_at_pause;
        //     timeline_slider.value = -24f + time - differenza_slide;
        // }

        // if (sm.tween.Duration () == 0) {
        //     differenza_slide = 0;
        // }

        float time = Time.time - PathCreator.time_started_play + PathCreator.pause_time;
        timeline_slider.value = -24f + time - differenza_slide;

        // Debug.Log("TIME: " + time.ToString() + " TIMESLIDER: " + timeline_slider.value.ToString());

    }

    public void SaveTimeline () {
        if (timelines_saved == null) {
            timelines_saved = new Dictionary<string, Timeline> ();
        }

        // List<Transform> PlayersForThisTimeline = new List<Transform> ();
        // foreach (Transform t in PlayersManager.GetHomePlayerInTheSceneList ())

        timelines_saved.Add (InputField.text, AnimationManager.GetTimeline ());
        Dropdown.ClearOptions ();
        Dropdown.AddOptions (GetListOfKeys (timelines_saved.Keys));
    }

    public void LoadTimeline () {

        CleanSceneOnLoad ();

        // //get the selected index
        // int menuIndex = Dropdown.GetComponent<Dropdown> ().value;

        // //get all options available within this dropdown menu
        // List<Dropdown.OptionData> menuOptions = Dropdown.GetComponent<Dropdown> ().options;

        // //get the string value of the selected index
        // string key = menuOptions[menuIndex].text;

        // Timeline t = timelines_saved[key];
        Timeline t = AnimationManager.timelineLoaded;
        AnimationManager.SetTimeline (t);
        PlayersManager.SpawnAfterLoad (t);
        PathCreator.OnLoadGianpaoloTimeline ();
        // aggiorno il timeline value all'ultimo kf inserito 

        //PathCreator.OnPlayPressed ();
        // PathCreator.Play ();
        // PathCreator.Pause ();

        // play = false;

        foreach (Frame f in t.frames) {
            AddMarkerOnLoad (f);
        }
    }

    public void AddMarkerOnLoad (Frame f) {
        GameObject s = Instantiate (keyframe_marker) as GameObject;

        float seconds = 24f - AnimationManager.TimeForThatFrame (f);

        float offset = (seconds) / (24) * (700f); //472

        // Y = (X-A)/(B-A) * (D-C) + C

        s.transform.position = new Vector3 (marker0_position.x + offset, timeline_slider.transform.position.y, 0);
        s.transform.parent = keyframes.transform;

    }

    public void NewTimeline () {

        // this.handle.position = this.marker0_position;

        Timeline t = new Timeline ();
        AnimationManager.SetTimeline (t);
        CleanSceneOnLoad ();

        Debug.Log ("NEW Timeline");

    }

    public void CleanSceneOnLoad () {
        // Elimino marker
        for (int i = 1; i < keyframes.childCount - 1; i++) {
            GameObject.Destroy (keyframes.GetChild (i).gameObject);
        }

        // Elimino giocatori in campo
        foreach (Transform p in PlayersManager.GetHomePlayerInTheSceneList ()) {
            //p.gameObject.SetActive(false);
            // GameObject.Destroy (p.gameObject);
        }

        // Pulisco la lista dei giocatori in campo
        // PlayersManager.SetHomeAwayPlayerInTheSceneList ();
        // Debug.Log (PlayersManager.GetHomePlayerInTheSceneList ().Count);

        // Sistemo i booleani per portarli allo stato iniziale
        this.play = false;
        PathCreator.alreadyPressed = false;
        PathCreator.ResetFirstTimePlay ();

        // Pulisco i path sotto il Waypoint Manager e per la palla
        for (int i = 0; i < PathCreator.transform.childCount; i++) {
            GameObject.Destroy (PathCreator.transform.GetChild (i).gameObject);
        }

        //GameObject.Destroy (Ball.GetComponent<splineMove> ());
    }

    // metodo per eliminare una timeline
    public void DeleteTimeline (string name) {
        timelines_saved.Remove (name);
    }

    // metodo per prendere la stringa da un menù a tendina
    private List<string> GetListOfKeys (Dictionary<string, Timeline>.KeyCollection Keys) {
        List<String> options = new List<string> ();
        foreach (string s in Keys) {
            options.Add (s);
        }
        return options;
    }

    // metodo NON USATO per incrementare la velocità
    public void IncrementSpeed () {
        foreach (Transform t in PlayersManager.GetHomePlayerInTheSceneList ()) {
            t.GetComponent<splineMove> ().OnChangeSpeedFactorSlider (speed_factor_slider.value);
        }
    }

    public void RemoveMarker (int i) {
        // Debug.Log ("I: " + i);
        GameObject go = keyframes.GetChild (i).gameObject;
        GameObject.Destroy (go);
    }

}