using System.Collections;
using System.Collections.Generic;
using Photon;
using UnityEngine;
using UnityEngine.Networking;

public class Manager3D : Photon.MonoBehaviour {

    //public AnimationManager AnimationManager;
    //public TimelineManager TimelineManager;
    [SerializeField] private PlayersManager PlayersManager;
    [SerializeField] private CameraManager CameraManager;
    [SerializeField] private Camera tribuna, defaultCamera;
    public Transform ball;
    // private PhotonView pv;

    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update () {

    }

    public void CallEditOnClients () {
        PhotonView photonView = PhotonView.Get (this);
        photonView.RPC ("EditMode", PhotonTargets.OthersBuffered);
    }

    [PunRPC]
    public void EditMode () {
        //accendo la camera in tribuna
        tribuna.gameObject.SetActive (true);
        tribuna.enabled = true;

        //metto i giocatori in 2D visti da alto

        List<Transform> giocatori = PlayersManager.GetHomePlayerInTheSceneList ();
        foreach (Transform t in giocatori) {
            t.GetChild (0).gameObject.SetActive (false);
            t.gameObject.GetComponent<MeshRenderer> ().enabled = true;
            // }
        }
    }

    public void CallActivate3DPlayerOnClients () {
        //Debug.Log ("IS: " + PhotonNetwork.isMasterClient);
        PhotonView photonView = PhotonView.Get (this);
        // photonView.RPC ("Activate3DPlayerOnClients", PhotonTargets.All);
        photonView.RPC ("Activate3DPlayerOnClients", PhotonTargets.All);
        // ActivateAndDisableMesh ();
    }

    // method to activate 3d players

    [PunRPC]
    public void Activate3DPlayerOnClients()
    {
        // if(!PhotonNetwork.isMasterClient){
        List<Transform> giocatori = PlayersManager.GetHomePlayerInTheSceneList();
        foreach (Transform t in giocatori)
        {
            // activate mesh of 3d player and deactivate the 2d one
            t.GetChild(0).gameObject.SetActive(!t.GetChild(0).gameObject.activeInHierarchy);
            t.gameObject.GetComponent<MeshRenderer>().enabled = !t.gameObject.GetComponent<MeshRenderer>().enabled;
        }
        // activate camera
        CameraManager.On3dSelect();
        // }
        PlayersManager.EnableDefence();
        ball.gameObject.GetComponent<MeshRenderer>().enabled = !ball.gameObject.GetComponent<MeshRenderer>().enabled;
    }

    private void ActivateAndDisableMesh () {
        List<Transform> giocatori = PlayersManager.GetHomePlayerInTheSceneList ();
        foreach (Transform t in giocatori) {
            t.GetChild (0).gameObject.SetActive (true);

            for (int i = 0; i < t.GetChild (0).childCount; i++) {
                t.GetChild (0).GetChild (i).gameObject.SetActive (false);
            }
        }
    }

}