﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtD : MonoBehaviour {

	public Transform giocatore;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
		this.transform.LookAt(giocatore);
		Vector3 rorotation = (this.giocatore.position-this.transform.position).normalized;
		transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(rorotation), Time.deltaTime);
	}
}
