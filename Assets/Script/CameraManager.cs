﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using UnityEngine.VR;

public class CameraManager : PunBehaviour
{

    [SerializeField] private Camera c1, c2, c3, c4, c5, defaultCamera, tribuna;
    private int cameraSelected;
    public List<Camera> camerasList { get; private set; }
    //private bool cameraSelected;
    [SerializeField] private CameraSelect CameraSelect;

    void Awake()
    {
        camerasList = new List<Camera>
        {
            c1,
            c2,
            c3,
            c4,
            c5,
            tribuna,
            defaultCamera
        };

        // set the mode VR or not according to the choice in the menu

        CameraSelect = GameObject.Find("CameraSelect").GetComponent<CameraSelect>();
        if (CameraSelect.GetCamera().Equals("Coach"))
        {
            DisableVR();
        }
    }

    // Use this for initialization
    void Start()
    {

    }


    // Update is called once per frame
    void Update()
    {

    }

    public override void OnJoinedRoom()
    {
        //if (PhotonNetwork.isMasterClient)
        //{
        //    DisableVR();
        //}
    }

    IEnumerator LoadDevice(string newDevice, bool enable)
    {
        VRSettings.LoadDeviceByName(newDevice);
        yield return null;
        VRSettings.enabled = enable;
    }

    void EnableVR()
    {
        StartCoroutine(LoadDevice("Cardboard", true));
    }

    void DisableVR()
    {
        StartCoroutine(LoadDevice("", false));
    }

    // method that fix the known bug (corrected now in unity 2018) that do not change the aspect ration when switching from VR to 2D

    public void OnAspectReset()
    {
        defaultCamera.ResetAspect();
    }

    public void On3dSelect()
    {
        if (PhotonNetwork.isMasterClient == false)
        {
            if (defaultCamera.enabled == true)
            {
                //Debug.Log("La camera era attivata");
                defaultCamera.enabled = false;
            }
            camerasList[CameraSelect.GetCameraInt() - 1].enabled = true;
        }
    }
}
