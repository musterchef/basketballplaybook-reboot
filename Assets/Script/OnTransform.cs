﻿using System;
using System.Collections;
using System.Collections.Generic;
using TouchScript.Behaviors;
using TouchScript.Gestures.TransformGestures;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Rendering;

// this class moves the ball when a player is moved and also draws and updates the lines of a player moved on the court according if the movement is a block, cut or dribble
public class OnTransform : MonoBehaviour {
    private TransformGesture p;
    private int lastKeyframe;
    [SerializeField] private AnimationManager AnimationManager;
    [SerializeField] private EventManager EventManager;

    [SerializeField] private bool alreadyCreated;
    [SerializeField] private bool alreadyMoved;

    [SerializeField] private GameObject clonePrefab;
    [SerializeField] private Ball ball;

    [SerializeField] private LineRenderer lineRenderer;

    [SerializeField] private Transform clones;

    [SerializeField] private Material[] materials;

    [SerializeField] private RectTransform pointVisualizer;
    [SerializeField] private RectTransform blockOrCut;

    [SerializeField] private Transform arrow;
    [SerializeField] private Transform block;

    [SerializeField] private Vector3 offset = new Vector3 (0.5f, 0, 0);

    public OnTransform(bool isReceiver) {
            this.isReceiver = isReceiver;  
        }
    public bool isReceiver { get; set; }

    // Use this for initialization
    void Start () {

        // initialization of the variables and assign the gesture p to its component

        lastKeyframe = 0;
        alreadyMoved = false;
        isReceiver = false;
        p = this.GetComponent<TransformGesture> ();

        // subscribe gesture to its event

        p.TransformStarted += ActivateClone;
        p.TransformCompleted += UpdateClone;
        p.Transformed += DrawArrow;
        p.Transformed += Dribble;
    }

    // Update is called every frame, if the MonoBehaviour is enabled.
    void Update()
    {
    }

    private void ActivateClone (object sender, EventArgs e) {

        // return if the gameobject is the ball because we don't need to do nothing with it for the moment
        if (this.name.Equals ("Ball")) {
            return;
        }

        // activate the LineRenderer
        lineRenderer.enabled = true;
        // activate the clone
        if (clones.childCount > 0) {
            clones.GetChild (clones.childCount - 1).gameObject.SetActive (true);
        }

        // check if the player holder is this gameObject
        if ((ball.playerHolder.name.Equals (this.gameObject.name)) && (!isReceiver))
        {
            // texture line renderer
            lineRenderer.sharedMaterial = materials[0];
        }
    }

    private void UpdateClone (object sender, EventArgs e) {

        // return if the gameobject is the ball because we don't need to do nothing with it for the moment

        if (this.name.Equals ("Ball")) {
            return;
        }

        // check if the coach selected block or cut movement and set the arrow correctly

        string playerHolder = ball.playerHolder.name;

        if ((!this.name.Equals (playerHolder) && (lastKeyframe > 0))) {

            // scale world coordinates to canvas' one! ;)

            RectTransform myRect = blockOrCut.GetComponent<RectTransform>();
            Vector2 myPositionOnScreen = Camera.main.WorldToScreenPoint(this.gameObject.transform.position);

            Canvas copyOfMainCanvas = GameObject.Find("Canvas").GetComponent<Canvas>();
            float scaleFactor = copyOfMainCanvas.scaleFactor;

            Vector2 finalPosition = new Vector2(myPositionOnScreen.x / scaleFactor, myPositionOnScreen.y / scaleFactor);
            myRect.anchoredPosition = finalPosition;

            EventManager.player = lineRenderer;
            blockOrCut.gameObject.SetActive (true);
        }

        // move and rotate the arrow

        if (clones.childCount > 0) {
            Vector3 start = clones.GetChild (clones.childCount - 1).transform.position;
            Vector3 end = this.transform.position;
            Vector3 vector = end - start;

            Vector3 newVector = vector.normalized;

            this.arrow.transform.position = this.transform.position + (newVector * -0.7f);
            this.arrow.LookAt (clones.GetChild (clones.childCount - 1).transform);
            this.arrow.Rotate (90f, 90f, 0f);

            this.block.transform.position = this.transform.position + (newVector * -0.4f);
            this.block.LookAt (clones.GetChild (clones.childCount - 1).transform);
            this.block.Rotate(0f, 90f, 0f);
        }

        // set the flag to true

        if (alreadyCreated == false) {
            alreadyCreated = true;
        }
    }


    // method to draw the line from the player to the destination 
    private void DrawArrow (object sender, EventArgs e) {

        if (this.name.Equals ("Ball")) {
            SetArrow (false);
            return;
        }

        if (this.isReceiver) {
            ball.DrawArrow ();
        }

        // draws the line
        lineRenderer.enabled = true;

        Timeline timeline = AnimationManager.GetTimeline ();

        int frames = timeline.frames.Count;
        string player = this.name;

        if (frames > 0) {
            if (clones.childCount > 0) {
                lineRenderer.SetPosition (0, new Vector3 (clones.GetChild (clones.childCount - 1).transform.position.x,
                    clones.GetChild (clones.childCount - 1).transform.position.y,
                    clones.GetChild (clones.childCount - 1).transform.position.z));
                lineRenderer.SetPosition (1, new Vector3 (this.transform.position.x, 0.3f, this.transform.position.z));
            }
        }

        // disable the ending part of the arrow while moving

        SetArrow (false);
        SetBlock (false);
    }

    private void Dribble (object sender, EventArgs e) {

        if (this.name.Equals ("Ball")) {
            return;
        }

        // check if this gameobject is the playerholder
        if (ball.playerHolder.name.Equals (this.gameObject.name)) 
        {
            ball.gameObject.transform.position = this.transform.position + offset;
        }
    }

    public void OnInsertKeyframe (int last) {

        // create the last keyframe placeholder but put it disabled (I use this in order to 

        if ((clones.childCount >= 0) && (last != lastKeyframe)) {

            lastKeyframe = last; // update the last kf

            Vector3 v = new Vector3 (this.transform.position.x, this.transform.position.y, this.transform.position.z);
            GameObject go = Instantiate (clonePrefab, v, new Quaternion (0f, 90f, 0f, 0f)) as GameObject;
            go.GetComponent<MeshRenderer>().receiveShadows = false;
            go.GetComponent<MeshRenderer>().shadowCastingMode = ShadowCastingMode.Off;
            go.name = this.name + "_KF_" + last;
            go.transform.parent = clones;
            go.gameObject.SetActive (false);
        }

        // reset boolean & update last keyframe

        lastKeyframe = last;
        alreadyMoved = false;

        // disable arrows

        SetArrow (false);
        SetBlock (false);

        // disable previous clones

        for (int i = 0; i < clones.childCount; i++) {
            clones.GetChild (i).gameObject.SetActive (false);
        }
    }

    public void OnReadKeyFrame (int index) {


        if (index != lastKeyframe - 1) {
            // disable all the clones

            for (int i = 0; i < clones.childCount - 1; i++) {
                clones.GetChild (i).gameObject.SetActive (false);
            }

            // the Player_Home_#_PN has to go in the clone's (index+1) position
            // at the same moment clone(index) activated
            // line from index to index+1 tracked

            this.transform.position = clones.GetChild (index + 1).position; // position 1
            Transform cloneToActivate = clones.GetChild (index);
            cloneToActivate.gameObject.SetActive (true); // position 0

            // LineRenderer Set

            lineRenderer.SetPosition (0, cloneToActivate.position);
            lineRenderer.SetPosition (1, this.transform.position);
            if (this.name.Equals ("Ball") == false)
                lineRenderer.enabled = true;

        } else  {
            for (int i = 0; i < clones.childCount - 1; i++) {
                clones.GetChild (i).gameObject.SetActive (false);
            }
            lineRenderer.enabled = false;
        }

    }

    public void SetArrow (bool active) {
        arrow.gameObject.SetActive (active);
    }

    public void SetBlock (bool active) {
        block.gameObject.SetActive (active);
    }
}