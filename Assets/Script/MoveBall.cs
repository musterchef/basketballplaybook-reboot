﻿using System;
using System.Collections;
using System.Collections.Generic;
using SWS;
using UnityEngine;

public class MoveBall : MonoBehaviour {

    [SerializeField] private MoveAnimator moveAnimator;
    [SerializeField] private PlayersManager playersManager;
    [SerializeField] private Transform motherBall;
    public string parameters { set; get; }

    // Use this for initialization
    void Start () { }

    // Update is called once per frame
    void Update () {

    }

    // metodo per passare la palla
    public IEnumerator LerpBall (Transform t, Vector3 startPos, float time, Transform p, MoveAnimator moveAnimator, MoveAnimator moveAnimatorPass) {

        //Debug.Log ("Sono dentro" + ": " + ma.name);

        this.motherBall.GetChild(0).GetComponent<MeshRenderer> ().enabled = true;

        double i = 0.0;
        var rate = 1.0 / time;
        while (i < 1.0) {
            if ((i > 0.50d) && (moveAnimator.GetPalleggio () == false)) {
                moveAnimator.SetLayerW(true);
                //Debug.Log ("cambio palleggiatore");
                OnPassaggioFalse();
                Ricezione (moveAnimator);
                moveAnimator.SetRicezione ();
                // ma.SetPalleggio (true);
            }
            i += Time.deltaTime * rate;
            motherBall.position = Vector3.Lerp (startPos, moveAnimator.GetMani (), (float) i);
            yield return null;
        }

        moveAnimatorPass.SetLayerW(false);

        // // disattivo la palla quando arriva nelle mani e attivo quella del giocatore che la riceve ;)

        // this.pallaMadre.GetComponent<MeshRenderer> ().enabled = false;
        // this.pallaMadre.GetChild (0).GetComponent<MeshRenderer> ().enabled = false;
        // p.GetComponent<MeshRenderer> ().enabled = true;
    }

    // metodo chiamando come evento durante l'animazione del passaggio e serve a lanciare il LerpBall per far sì
    // che la palla vada a finire nelle mani del giocatore che è il ricevitore

    public void MetodoEvento () {
        // sistemo i parametri che arrivano tutti dentro una stringa
        string[] ps = parameters.Split (';');
        float duration = float.Parse (ps[2]);
        MoveAnimator moveAnimatorPass = playersManager.playermap[ps[0]].GetComponent<MoveAnimator> ();
        MoveAnimator moveAnimatorReceiver = playersManager.playermap[ps[1]].GetComponent<MoveAnimator> ();
        Transform p = playersManager.playermap[ps[1]].GetComponent<MoveAnimator> ().palla.transform;
        // spengo la palla del giocatore che sta per passare e accendo quella che deve partire nel LerpBall
        //this.GetComponent<Renderer> ().enabled = false;

        PhotonView.Get(this).RPC("HideBall", PhotonTargets.AllBuffered);

        motherBall.position = playersManager.playermap[ps[0]].GetComponent<MoveAnimator> ().palla.transform.position;
        // Debug.Log ("pallaMadre: " + pallaMadre.position);
        moveAnimatorPass.SetPalleggio (false);
        StartCoroutine (LerpBall (motherBall, motherBall.position, duration, p, moveAnimatorReceiver, moveAnimatorPass));
    }

    [PunRPC]
    public void HideBall()
    {
        this.GetComponent<Renderer>().enabled = false;
    }

    public void Ricezione (MoveAnimator ma) {
        MoveB mb = motherBall.GetComponent<MoveB> ();
        mb.moveAnimator = ma;
        mb.inMano = true;
    }

    public void OnPassaggioFalse(){
        this.transform.parent.transform.parent.GetComponent<MoveAnimator>().onPassaggio = false;
    }

    // public void InizializzaPassaggio () {
    //     passStarted = true;
    //     this.pallaMadre.GetComponent<Renderer> ().enabled = true;
    //     this.GetComponent<Renderer> ().enabled = false;
    // }
}