﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timeline {
    public List<Frame> frames { get; set; }

    public Timeline () {
        frames = new List<Frame> ();
    }

    // method used in order to put the events started in the Event List 
    public void AddEventStartInTheEventList (List<Evento> event_list, DateTime tstamp, DateTime today, float duration) {

        foreach (Evento e in event_list) {
            Evento evento = new Evento (e.id_type, e.id_event, true, e.idPlayer1, e.idPlayer2, e.duration, e.keyframe);
            frames[frames.Count - 2].event_list.Add (evento);
        }

        //     // passo lista eventi da aggiungere, timestamp dell'evento effettuato, timestamp today che mi serve per calcolare i secondi passati
        //     // ...e duration che sarebbe la differenza di tempo da quando l'evento è iniziato a quando è finito                                                            
        //     int s = (today.Subtract (tstamp).Seconds - duration);
        //     // s = secondi sul cronometro quando è iniziato l'evento
        //     foreach (Frame f in frames) {
        //         int t = today.Subtract (f.timeStamp).Seconds;
        //         // t = secondi del cronometro al frame f
        //         if (t == s) { // cerco quello dove devo aggiungere gli eventi
        //             foreach (Evento e in event_list) {
        //                 if ((e.id_type.Equals ("passaggio")) || // o passaggio o blocco
        //                     (e.id_type.Equals ("blocco"))) {
        //                     Evento b = new Evento (e.id_type, e.id_event, true, e.idPlayer1, e.idPlayer2, e.duration, e.keyframe);
        //                     f.event_list.Add (b);
        //                 }
        //             }
        //         }
        //     }
        // }
    }
}

public class Frame {
    public DateTime timeStamp { get; set; }
    public List<PlayerInfo> players_home { get; set; }
    public List<PlayerInfo> players_away { get; set; }
    public List<Evento> event_list { set; get; }
    public Dictionary<string, Evento> event_map { set; get; }
    public Transform playerHolder { set; get; }
    public Vector3 ball_info;
    public Frame (DateTime time, List<PlayerInfo> players_home, List<PlayerInfo> players_away, Vector3 ball_info, List<Evento> event_list, Transform playerHolder) {
        this.timeStamp = time;
        this.players_home = players_home;
        this.players_away = players_away;
        this.ball_info = ball_info;
        this.event_list = event_list;
        this.playerHolder = playerHolder;
    }

}

public class PlayerInfo {
    public string idPlayer { get; set; }
    public float x { get; set; }
    public float y { get; set; }
    public float z { get; set; }

    public PlayerInfo (string id, float x, float z, float y) {
        this.idPlayer = id;
        this.x = x;
        this.y = y;
        this.z = z;
    }
}

public class Evento {
    public string id_type { get; set; }
    public string idPlayer1 { get; set; }
    public string idPlayer2 { get; set; }
    public float duration { get; set; }
    public int id_event { get; set; }
    public int keyframe { get; set; }

    public bool startend { get; set; }
    public Evento (string id_type, int id_event, bool startend, string idPlayer1, string idPlayer2, float duration, int keyframe) {
        this.id_type = id_type;
        this.id_event = id_event;
        this.startend = startend;
        this.idPlayer1 = idPlayer1;
        this.idPlayer2 = idPlayer2;
        this.duration = duration;
        this.keyframe = keyframe;
    }

    public void UpdateEvento (string id_type) {
        this.id_type = id_type;
    }
}