﻿using System;
using System.Collections;
using System.Collections.Generic;
using SWS;
using TouchScript.Gestures.TransformGestures;
using UnityEngine;
using UnityEngine.Networking;

public class Ball : MonoBehaviour {
    private TransformGesture t;
    private GameObject Contenitore;
    private PlayersManager PlayersManager;
    private AnimationManager AnimationManager;
    private EventManager EventManager;
    public Transform oldPlayerHolder { get; set; }
    public Transform playerHolder { get; set; }
    public Vector3 offset = new Vector3 (0.5f, 0f, 0f);
    [SerializeField] private OnTransform onTransform;
    [SerializeField] private LineRenderer lineRenderer;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake () {
        EventManager = GameObject.Find ("EventsManager").GetComponent<EventManager> ();
        PlayersManager = GameObject.Find ("PlayersManager").GetComponent<PlayersManager> ();
        AnimationManager = GameObject.Find ("AnimationManager").GetComponent<AnimationManager> ();
        Contenitore = GameObject.Find ("Contenitore");
        t = this.GetComponent<TransformGesture> ();

        playerHolder = this.transform; // set the ball as the first playerHolder assuming at the beginning that no player is holding the ball
        t.TransformCompleted += FindBall; // when the ball is moved, it snaps to the nearest player (see the method below CercarePalla
    }

    // Use this for initialization
    void Start () { }

    // Update is called once per frame
    void Update () { }

    // Method to find who is holding the ball now
    public Transform GetPlayerHolder (PlayersManager pm) {
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (Transform potentialTarget in pm.GetHomePlayerInTheSceneList ()) {
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr) {
                closestDistanceSqr = dSqrToTarget;
                playerHolder = potentialTarget;
            }
        }
        // Debug.Log (playerHolder.name);
        return playerHolder;
    }
    private void FindBall (object sender, EventArgs e) {
        oldPlayerHolder = playerHolder;
        this.transform.position = GetPlayerHolder (PlayersManager).position + offset;
        //this.transform.parent = playerHolder.transform; // faccio così per fare in modo che se sposto il giocatore con la palla, essa si sposta con lui
        // p = playerHolder.gameObject.GetComponent<TransformGesture>();
        DetectPass ();
    }

    // method that detects if there a pass between two players, but in different keyframes! ...And create an Event
    private void DetectPass () {
        if ((this.oldPlayerHolder != this.playerHolder) &&
            (AnimationManager.sliderSeconds != AnimationManager.lastKeyFrameSeconds) &&
            AnimationManager.lastKeyFrameSeconds != 0) // vedo se è cambiato il portatore palla e se i keyframes sono diversi
        {
            // set dell'evento
            int frame_duration = AnimationManager.sliderSeconds - AnimationManager.lastKeyFrameSeconds;
            int keyframe = AnimationManager.GetTimeline ().frames.Count;

            Timeline timeline = AnimationManager.GetTimeline ();
            Transform holder = timeline.frames[timeline.frames.Count - 1].playerHolder;

            Evento e = new Evento ("passaggio", EventManager.id_event, false, holder.name, this.playerHolder.name, frame_duration, keyframe);
            EventManager.AddToCurrentEvents (e);
            Debug.Log ("Passaggio effettuato da: " + holder.name + " a: " + this.playerHolder);

            // // set del LineRenderer
            // // set della freccia

            DrawArrow ();

            // metto ON il flag del ricevitore

            foreach (Transform t in PlayersManager.GetHomePlayerInTheSceneList ()) {
                t.GetComponent<OnTransform> ().isReceiver = false;
            }

            playerHolder.GetComponent<OnTransform> ().isReceiver = true;

        }
    }

    public void DrawArrow () {
        Timeline timeline = AnimationManager.GetTimeline ();
        Transform holder = timeline.frames[timeline.frames.Count - 1].playerHolder;
        // set del LineRenderer

        // Vector3 oldP = this.oldPlayerHolder.position;
        Vector3 oldP = holder.position;
        Vector3 newP = this.playerHolder.position;

        this.lineRenderer.SetPosition (0, oldP);
        this.lineRenderer.SetPosition (1, newP);
        this.lineRenderer.enabled = true;

        // set della freccia

        Vector3 start = oldP;
        Vector3 end = newP;
        Vector3 vector = end - start;

        Vector3 newVector = vector.normalized;

        Transform freccia = this.transform.GetChild (1);
        Transform blocco = this.transform.GetChild (2);

        freccia.transform.position = newP + (newVector * -0.7f);
        freccia.LookAt (holder);
        freccia.Rotate (90f, 90f, 0f);
        freccia.gameObject.SetActive (true);
    }

    public void Shot () {
        this.transform.position = this.transform.parent.transform.parent.GetChild (0).position; // sposto la palla sotto il canestro
        int frame_duration = AnimationManager.sliderSeconds - AnimationManager.lastKeyFrameSeconds;
        int keyframe = AnimationManager.GetTimeline ().frames.Count;
        Evento e = new Evento ("tiro", EventManager.id_event, true, this.oldPlayerHolder.name, this.playerHolder.name, frame_duration, keyframe); //aggiungo l'evento tiro nella lista
        EventManager.AddToCurrentEvents (e);
    }

    public void SetLineRenderer () {
        this.lineRenderer.enabled = false;
    }
}