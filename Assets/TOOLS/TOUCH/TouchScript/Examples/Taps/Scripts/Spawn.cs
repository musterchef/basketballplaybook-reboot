﻿/*
 * @author Valentin Simonov / http://va.lent.in/
 */

using System;
using Photon;
using TouchScript.Gestures;
using TouchScript.Hit;
using UnityEngine;
using UnityEngine.Networking;

namespace TouchScript.Examples.Tap {
    public class Spawn : Photon.MonoBehaviour {
        public Transform CubePrefab;
        public Transform Container, Ghosts;
        public Material one, two, three, four, five;
        public Material[] materials;
        public float Scale = .5f;

        ////// Edited 
        private string home_away;
        // use this to store the players in a list of players
        public PlayersManager pm;

        // Use this for initialization
        void Start () {
            InitializeSpawn (); // inserisco i giocatori nella lista dei giocatori
        }

        private void OnEnable () {
            GetComponent<TapGesture> ().Tapped += tappedHandler;
        }

        private void OnDisable () {
            GetComponent<TapGesture> ().Tapped -= tappedHandler;
        }

        private void tappedHandler (object sender, EventArgs e) {
            var gesture = sender as TapGesture;
            HitData hit = gesture.GetScreenPositionHitData ();
            SpawnPlayer (hit);
        }

        // change prefab according to home or away
        public void setCubePrefab (Transform prefab) {
            CubePrefab = prefab;
        }

        // change gameobject name according to home or away
        private string HomeAwayString () {
            if (pm.home_away == true) {
                home_away = "Home";
            } else {
                home_away = "Away";
            }
            return home_away;
        }

        // method to spawn the player in the scene
        public void SpawnPlayer (HitData hit) {
            if ((pm.home_away == true) &&
                (pm.GetHomePlayerInTheSceneList ().Count < 5)) { // this is the home/away check + the check if there are already 5 players in the court
                pm.AddPlayerToHomePlayersList (InstantiatePlayer (hit, pm.home_away,
                    pm.GetHomePlayerInTheSceneList ().Count));
            } else
            if ((pm.home_away == false) &&
                (pm.GetAwayPlayerInTheSceneList ().Count < 5)) {
                pm.AddPlayerToAwayPlayersList (InstantiatePlayer (hit, pm.home_away,
                    pm.GetAwayPlayerInTheSceneList ().Count));
            }
        }

        // method to instantiate the gameobject of the prefab (now there is a cube, in future the mesh of the players) in the position given by the hit data from the tap gesture
        public Transform InstantiatePlayer (HitData hit, bool ha, int index) {
            var cube = Instantiate (CubePrefab) as Transform;
            cube.parent = Container;
            /// EDITED
            cube.name = "Player_" + HomeAwayString () + "_" + (index + 1).ToString ();
            ///
            cube.localScale = Vector3.one * Scale * cube.localScale.x;
            cube.position = hit.Point + hit.Normal * .5f;
            cube.position = new Vector3 (cube.position.x, 0f, cube.position.z);

            // CHECK IF THERE IS CONNECTION
            NetworkServer.Spawn (cube.gameObject);

            return cube;
        }

        // method to spawn after a timeline is loaded        
        public void SpawnLoad (string id, Vector3 position) {
            var cube = Instantiate (CubePrefab) as Transform;
            cube.name = id;
            cube.position = position;
            cube.parent = Container;
            pm.AddPlayerToHomePlayersList (cube);

            // CHECK IF THERE IS CONNECTION
            NetworkServer.Spawn (cube.gameObject);
        }

        // metotdo per inserire i giocatori in campo nelle rispettive liste utili alla gestione keyframe etc...
        public void InitializeSpawn () {

            for (int i = 0; i < Container.childCount; i++) {

                // ADD PLAYER TO THE LIST
                if (i < 5) {

                    // SETTO ATTIVI I GIOCATORI GIÀ PIAZZATI IN CAMPO
                    Container.GetChild (i).gameObject.SetActive (true);
                    pm.AddPlayerToHomePlayersList (Container.GetChild (i).transform);
                    // Debug.Log ("Added: " + Container.GetChild (i).name + " ---> " + pm.GetHomePlayerInTheSceneList ().Count.ToString ());

                } else if ((i > 4) && (i < Container.childCount - 1)) {
                    pm.AddPlayerToAwayPlayersList (Container.GetChild (i).transform);
                }
            }

            pm.Ball.transform.position = pm.Ball.GetPlayerHolder (pm).position + new Vector3 (0.5f, 0f, 0f);

            GetComponent<TapGesture> ().Tapped -= tappedHandler; // disattivato il tap dopo aver inserito i giocatori in lista

        }
    }
}