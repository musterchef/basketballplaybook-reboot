﻿/* This file is part of the "Simple Waypoint System" project by Rebound Games.
 * You are only allowed to use these resources if you've bought them from the Unity Asset Store.
 *  You shall not license, sublicense, sell, resell, transfer, assign, distribute or
 *  otherwise make available to any third party the Service or the Content. */

using DG.Tweening;
using UnityEngine;
// #if UNITY_5_5_OR_NEWER
using System.Collections;
using UnityEngine.AI;
// #endif

namespace SWS {
    /// <summary>
    /// Mecanim motion animator for movement scripts.
    /// Passes speed and direction to the Mecanim controller.
    /// <summary>
    public class MoveAnimator : Photon.MonoBehaviour {
        //movement script references
        [SerializeField] private splineMove sMove;
        [SerializeField] private Animator pallone;
        [SerializeField] private Transform manoDX, manoSX, motherBall;
        [SerializeField] private Animator animator;
        [SerializeField] private Transform tiroManager;
        public Transform canestro;
        public bool onPassaggio { get; set; }
        public bool onTiro { get; set; }

        public bool onRicezione { get; set; }

        private Transform ricevitore;
        private Transform passatore;
        private NavMeshAgent nAgent;
        private splineMove shotSplineMove;

        //Mecanim animator reference

        //cached y-rotation on tweens
        private float lastRotY;
        public Animator palla { private set; get; }

        //getting component references
        void Start () {
            palla = pallone;
            onPassaggio = false;
            onRicezione = false;
            onTiro = false;
            shotSplineMove = null;
            // animator = GetComponentInChildren<Animator>();

            // sMove = GetComponent<splineMove>();
            // if (!sMove)
            // nAgent = GetComponent<NavMeshAgent>();

        }

        /// Update is called every frame, if the MonoBehaviour is enabled.
        void Update () { }

        //method override for root motion on the animator
        void OnAnimatorMove () {

            // if ((this.animator.GetBool ("palleggio")) ||
            //     (this.animator.GetBool ("passaggio")) ||
            //     (this.animator.GetBool ("tiro"))
            // ) {
            //     Debug.Log("peso delle mani messo a 1");
            //     this.animator.SetLayerWeight (1, 1);
            // } else {
            //     Debug.Log("peso delle mani messo a 0");
            //     this.animator.SetLayerWeight (1, 0);
            // }

            //init variables
            float speed = 0;
            float angle = 0;

            //calculate variables based on movement script:
            //for splineMove and bezierMove, speed and rotation are regulated by the tween.
            //here we just get the tween's speed and calculate the rotation difference to the last frame.
            //navmesh agents have their own type of movement which has to be calculated separately.
            if (sMove) {
                speed = sMove.tween == null || !sMove.tween.IsPlaying () ? 0 : sMove.speed;
                // angle = (transform.eulerAngles.y - lastRotY) * 10;
                // lastRotY = transform.eulerAngles.y;
            } else {
                speed = nAgent.velocity.magnitude;
                Vector3 velocity = Quaternion.Inverse (transform.rotation) * nAgent.desiredVelocity;
                angle = Mathf.Atan2 (velocity.x, velocity.z) * 180.0f / 3.14159f;
            }

            // calcolo la rotazione del corpo rispetto al prossimo waypoint

            if ((sMove.waypoints.Length > 0) && ((sMove.currentPoint != sMove.waypoints.Length - 1))) {

                if (onPassaggio) {
                    Vector3 d = (ricevitore.position - this.transform.position).normalized;
                    Quaternion toRt = Quaternion.LookRotation (d);
                    Quaternion curRt = Quaternion.Slerp (this.gameObject.transform.rotation, toRt, Time.deltaTime * 1.2f);
                    this.gameObject.transform.rotation = curRt;
                    return;
                }

                // if (onRicezione) {
                //     Debug.Log ("STO GIRANDO x RICEVERE");
                //     Vector3 d = (this.transform.position-pallaG.transform.position).normalized;
                //     Quaternion toRt = Quaternion.LookRotation (d);
                //     Quaternion curRt = Quaternion.Slerp (this.gameObject.transform.rotation, toRt, Time.deltaTime * 1.2f);
                //     this.gameObject.transform.rotation = curRt;
                //     return;
                // }

                Vector3 wp1 = sMove.waypoints[sMove.currentPoint];
                Vector3 wp2 = sMove.waypoints[sMove.currentPoint + 1];
                Vector3 dir = (wp2 - wp1).normalized;

                Quaternion toRot = Quaternion.LookRotation (dir);
                Quaternion curRot = Quaternion.Slerp (this.gameObject.transform.rotation, toRot, Time.deltaTime * 1.2f);
                this.gameObject.transform.rotation = curRot;
                // float y = curRot.eulerAngles.y;
                // animator.SetFloat ("Turn", y, 0.15f, Time.deltaTime);
                // this.gameObject.transform.rotation = Quaternion.LookRotation (dir);
            }

            //push variables to the animator with some optional damping
            animator.SetFloat ("Forward", speed);
            // animator.SetFloat ("Turn", angle, 0.15f, Time.deltaTime);
        }


        public void SetPalleggio (bool b) {
            this.animator.SetBool ("palleggio", b);
            this.pallone.SetBool ("palleggio", b);
        }
        public IEnumerator SetPalleggioAfterReceive (bool b) {
            this.animator.SetBool ("palleggio", b);
            yield return new WaitForSeconds (0.25f); // il tempo che esce dalla ricezione
            this.pallone.SetBool ("palleggio", b);
            StopReceive ();
        }
        public void SetLayerW (bool b) {
            if (b) {
                this.animator.SetLayerWeight (1, 1);
            }
            if (!b) {
                this.animator.SetLayerWeight (1, 0);
            }
        }
        public void SetPassaggio (Transform transform) {
            this.animator.SetTrigger ("passaggio");
            this.pallone.SetTrigger ("passaggio");
            this.animator.SetBool ("palleggio", false);
            ricevitore = transform;
            onPassaggio = true;
        }
        public void SetTiro (Vector3 canestro, Vector3 tiratore, float time) {
            // this.animator.SetTrigger ("tiro");
            // this.SetPalleggio(false);
            StartCoroutine (Tiro (canestro, tiratore, time));
            onTiro = true;
        }

        public IEnumerator Tiro (Vector3 basket, Vector3 tiratore, float time) {
            double i = 0.0;
            var rate = 1.0 / time;
            while (i < 1.0) {
                i += Time.deltaTime * rate;
                Vector3 d = (basket - this.transform.position).normalized;
                Quaternion desiredRotation = Quaternion.LookRotation (d);
                Quaternion currentRotation = Quaternion.Slerp (this.gameObject.transform.rotation, desiredRotation, (float) i*100f);
                this.gameObject.transform.rotation = new Quaternion (0, currentRotation.y, 0, 0);
                yield return null;
            }
            PhotonView photonView = PhotonView.Get(this);
            photonView.RPC("SetShoot", PhotonTargets.AllBuffered);
            //SetShoot();
        }

        [PunRPC]
        public void SetShoot () {
            // sistemo i trigger e abilito e disabilito le mesh che mi servono
            this.animator.SetTrigger ("tiro");
            this.SetPalleggio (false);
            this.pallone.GetComponent<Renderer> ().enabled = false;
            this.motherBall.GetComponent<MoveB> ().inMano = true;
            this.motherBall.transform.GetChild (0).GetComponent<Renderer> ().enabled = true;
        }

        public void SetRicezione () {
            this.animator.SetTrigger ("ricezione");
            onRicezione = true;
        }

        public bool GetPalleggio () {
            return this.animator.GetBool ("palleggio");
        }
        public Vector3 GetMani () {
            return ((manoDX.position + manoSX.position) / 2);
        }
        public Transform GetManoDX () {
            return manoDX;
        }
        public Transform GetManoSX () {
            return manoSX;
        }

        public void PlayPalleggio () {
            this.animator.SetBool ("palleggio", true);
            this.palla.SetBool ("palleggio", true);
        }
        public void EPoiBum () {

            if (tiroManager.childCount == 0)
            {
                // create path to make the shot do a parabola

                // trovo prima un punto intermedio tra giocatore e canestro per fare la parabola
                //  A+(B-A)/2

                Vector3 pallaposition = motherBall.transform.position;

                float ppx = pallaposition.x;
                float ppz = pallaposition.z;

                float cpx = this.canestro.position.x;
                float cpz = this.canestro.position.z;

                float midposition_x = ppx + (cpx - ppx) / 2;
                float midposition_z = ppz + (cpz - ppz) / 2;

                Vector3 midposition = new Vector3(midposition_x, 3.40f, midposition_z);

                shotSplineMove = this.motherBall.gameObject.AddComponent<splineMove>();

                GameObject tiro0, tiro1, tiro2;

                tiro0 = new GameObject("tiro0");
                tiro0.transform.position = pallaposition;
                tiro0.transform.parent = tiroManager;

                tiro1 = new GameObject("tiro1");
                tiro1.transform.position = midposition;
                tiro1.transform.parent = tiroManager;

                tiro2 = new GameObject("tiro2");
                tiro2.transform.position = canestro.position;
                tiro2.transform.parent = tiroManager;

                PathManager pathManager = tiroManager.gameObject.AddComponent<PathManager>();
                pathManager.Create();
                shotSplineMove.pathContainer = pathManager;
            }
            motherBall.GetComponent<MoveB> ().inMano = false;
            shotSplineMove.StartMove ();
            shotSplineMove.tween.OnComplete (ActivatePhysicsOnTheBall);
        }

        public void ActivatePhysicsOnTheBall () {
            if ((motherBall.gameObject.GetComponent<Rigidbody> () == null) & (motherBall.gameObject.GetComponent<SphereCollider> () == null)) {
                motherBall.gameObject.AddComponent<Rigidbody> ();
                motherBall.gameObject.AddComponent<SphereCollider> ();
            } else {
                motherBall.gameObject.GetComponent<Rigidbody> ().isKinematic = false;
                motherBall.gameObject.GetComponent<SphereCollider>().enabled = true;
            }
        }
        public void DisablePhysics () {
            if ((motherBall.gameObject.GetComponent<Rigidbody> () != null) && (motherBall.gameObject.GetComponent<SphereCollider> () != null)) {
                motherBall.gameObject.GetComponent<Rigidbody> ().isKinematic = true;
                motherBall.gameObject.GetComponent<SphereCollider>().enabled = false;
            }
        }

        public void ReceiveAndDrill () {
            // Debug.Log ("RECEIVE AND DRILL");
            // SetPalleggio (true);
            StartCoroutine (SetPalleggioAfterReceive (true));
        }
        public void StopReceive () {
            pallone.GetComponent<MeshRenderer> ().enabled = true;
            motherBall.GetChild (0).GetComponent<MeshRenderer> ().enabled = false;
            motherBall.GetComponent<MeshRenderer> ().enabled = false;
        }

        // public IEnumerator LaunchBall () {

        //     float _angle = 45f;

        //     // source and target positions
        //     Vector3 pos = transform.position;
        //     Vector3 target = canestro.position;

        //     // distance between target and source
        //     float dist = Vector3.Distance (pos, target);

        //     while (dist <

        //     // rotate the object to face the target
        //     transform.LookAt (target);

        //     // calculate initival velocity required to land the cube on target using the formula (9)
        //     float Vi = Mathf.Sqrt (dist * -Physics.gravity.y / (Mathf.Sin (Mathf.Deg2Rad * _angle * 2)));
        //     float Vy, Vz; // y,z components of the initial velocity

        //     Vy = Vi * Mathf.Sin (Mathf.Deg2Rad * _angle);
        //     Vz = Vi * Mathf.Cos (Mathf.Deg2Rad * _angle);

        //     // create the velocity vector in local space
        //     Vector3 localVelocity = new Vector3 (0f, Vy, Vz);

        //     // transform it to global vector
        //     Vector3 globalVelocity = transform.TransformVector (localVelocity);

        //     // launch the cube by setting its initial velocity
        //     GetComponent<Rigidbody> ().velocity = globalVelocity;

        //     // // after launch revert the switch
        //     // _targetReady = false;
        // }



        // ricezione

    }
}